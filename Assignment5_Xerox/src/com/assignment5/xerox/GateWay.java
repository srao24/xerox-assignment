/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.xerox;

import com.assignment5.analytics.AnalysisHelper;
import com.assignment5.analytics.Datastore;
import com.assignment5.entities.Item;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import java.io.IOException;
import java.util.Map;

/**
 *
 * @author kasai
 */
public class GateWay {
    
    AnalysisHelper helper;
    DataReader productReader;
    DataReader orderReader;
    
    public GateWay() throws IOException{
         DataGenerator generator = DataGenerator.getInstance();
        productReader = new DataReader(generator.getProductCataloguePath());
        orderReader = new DataReader(generator.getOrderFilePath());
        helper = new AnalysisHelper();
    }
    
    public static void main(String args[]) throws IOException{
        GateWay gate = new GateWay();
   
        System.out.println("_____________________________________________________________");
        gate.readData();
        
      
    }
    public void readData() throws IOException{
     String[] prodRow;
       // printRow(productReader.getFileHeader());
        while((prodRow = productReader.getNextRow()) != null){
           generateProduct(prodRow);
            
    } 
        String[] orderRow;
        
        while((orderRow = orderReader.getNextRow()) != null){
           
            generateOrder(orderRow);
        }runAnalysis();
    }
    
    private  void generateProduct(String[] row){
        int productId = Integer.parseInt(row[0]);
        int minPrice = Integer.parseInt(row[1]);
         int maxPrice = Integer.parseInt(row[2]);
          int targetPrice = Integer.parseInt(row[3]);
        Product product = new Product(productId, minPrice, maxPrice, targetPrice);
        Datastore.getInstance().getProducts().put(productId, product);
        
    }
    private Order generateOrder(String[] row){
         int orderId = Integer.parseInt(row[0]);
        int salesId = Integer.parseInt(row[4]);
        int customerId = Integer.parseInt(row[5]);
        int productId = Integer.parseInt(row[2]);
        int salesPrice = Integer.parseInt(row[6]);
        int quantity = Integer.parseInt(row[3]);
        
        Item item = new Item(productId, salesPrice, quantity);
        
        Order orderObj = new Order(orderId, salesId, customerId, item);
        Datastore.getInstance().getOrders().put(orderId, orderObj);
     
        //Users who created it update that
        
        Map<Integer, Product> product = Datastore.getInstance().getProducts();
        if(product.containsKey(productId)){
            product.get(productId).getOrders().add(orderObj);
        }
        
        
        return null;
    }
    
     private void runAnalysis() {
         
         System.out.println("_____________________________________________________________________________________________");
         System.out.println("Question number 1 - Top 3 Best Negotiated");
         helper.topNegotiatedProducts();
         
         System.out.println("_____________________________________________________________________________________________");
         System.out.println("Question number 1 - Top 3 Best Negotiated with tie");
         helper.topNegotiatedProdwithTie();
         System.out.println("_____________________________________________________________________________________________");
         System.out.println("Question number 2 - Top 3 best customers");
         helper.topThreeCustomers();
         System.out.println("______________________________________________________________________________________________");
         System.out.println("Question number 3 - Top 3 best sales people");
         helper.topThreeSalesPeople();
         System.out.println("______________________________________________________________________________________________");
         System.out.println("Question number 5a - Display Table for the original data\n");
         helper.displayAverageSalesPrice();
         System.out.println("______________________________________________________________________________________________");
         System.out.println("Question number 5b -Approach 1\n");
         helper.modification();
         System.out.println("______________________________________________________________________________________________");
         System.out.println("Question number 5b - Approach 2");
         helper.dispayModifiedDataApproach2();
         System.out.println("______________________________________________________________________________________________");
         System.out.println("Question number 5b - Approach 3");
         helper.modifiedtargetvalue3();
         System.out.println("______________________________________________________________________________________________");
         
     }
    
    
}
