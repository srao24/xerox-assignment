/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.ModifiedTable;
import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import com.assignment5.entities.Table;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sid
 */
public class AnalysisHelper {
    
    
    
    public void topNegotiatedProducts(){
                
        Map<Integer, Order> order = Datastore.getInstance().getOrders(); // this gets order map that we stored using datastore
        List<Order> orderList = new ArrayList<>(order.values());
        Map<Integer, Product> product = Datastore.getInstance().getProducts(); // this gets product maps which we stored using datastore
      
        Map<Integer, Integer> prod = new HashMap<>();
       int quantity = 0;
       
       //Order --> Item --> Product id
for(Order o: order.values()){
  //  System.out.println(o.getItem().getProductId());
Product p = product.get(o.getItem().getProductId());
 //  System.out.println(p.getTargetPrice());
    if(o.getItem().getSalesPrice()>p.getTargetPrice()){
     if(prod.containsKey(o.getItem().getProductId())){
       
         quantity=o.getItem().getQuantity()+prod.get(o.getItem().getProductId());
     }
             else{
         quantity=o.getItem().getQuantity();
     }
     prod.put(p.getProductId(), quantity);
 }
  
     
        
}
        List<Map.Entry<Integer, Integer>> prodList = new LinkedList<Map.Entry<Integer, Integer>>(prod.entrySet());   
        Collections.sort(prodList, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
               return Double.compare(t1.getValue(), t.getValue());
            }
          
        });
        System.out.println("Product Id"+"\t"+"Sales Quantity sold above target price");
        for(int i=0;i<prodList.size()&&i<3;i++){
            
              System.out.println(prodList.get(i).getKey()+"\t\t"+prodList.get(i).getValue());
        }
    }
  //Code for matching tie values
    
    public void topNegotiatedProdwithTie(){
//                
        Map<Integer, Order> order = Datastore.getInstance().getOrders(); // this gets order map that we stored using datastore
        List<Order> orderList = new ArrayList<>(order.values());
        Map<Integer, Product> product = Datastore.getInstance().getProducts(); // this gets product maps which we stored using datastore
//       
        Map<Integer, Integer> prod = new HashMap<>();
       int quantity = 0;
       
       //Order --> Item --> Product id
for(Order o: order.values()){

Product p = product.get(o.getItem().getProductId());

    if(o.getItem().getSalesPrice()>p.getTargetPrice()){
     if(prod.containsKey(o.getItem().getProductId())){
       
         quantity=o.getItem().getQuantity()+prod.get(o.getItem().getProductId());
     }
             else{
         quantity=o.getItem().getQuantity();
     }
     prod.put(p.getProductId(), quantity);
 }
  
     
        
}
        List<Map.Entry<Integer, Integer>> prodList = new LinkedList<Map.Entry<Integer, Integer>>(prod.entrySet());   
        Collections.sort(prodList, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
               return Double.compare(t1.getValue(), t.getValue());
            }
          
        });
        System.out.println("Product Id"+"\t"+"Sales Quantity sold above target price");
      int max =0;
      int key = 0;
      int count =0;
      int groupCount = 0;
        for(int i=0;i<prodList.size()&&groupCount<=3;){
            
            
            if(i==0){
                System.out.println(prodList.get(i).getKey()+"\t\t"+prodList.get(i).getValue());
                max = prodList.get(i).getValue();
                key = prodList.get(i).getKey();
            i++;
            }
            if(i>0 ){
                 
                if(prodList.get(i).getValue() == max && prodList.get(i).getKey()!=key){
                   
                    System.out.println(prodList.get(i).getKey()+"\t\t"+prodList.get(i).getValue());
                  key = prodList.get(i).getKey();
                if(count==0 ){
                    
                    i++;
                    if(groupCount>=3){
                        break;
                    }
                    
                }
       
                 count++;
                }
                else{
                    if(count!=0){
                        System.out.println("-------------------------------------");
                        count=0;
                    }else if (groupCount>=2){
                        break;
                    }
                        groupCount++;
                    System.out.println(prodList.get(i).getKey()+"\t\t"+prodList.get(i).getValue());
                   max = prodList.get(i).getValue();
                   key = prodList.get(i).getKey();
           i++;
                }
            }
             
            
//            if(i=0){
//            System.out.println(prodList.get(i).getKey()+"\t\t"+prodList.get(i).getValue());
//            }
//            
////                if(prodList.get(i).getValue().equals(prodList.get(i-1).getValue())){
////                    
////                    System.out.println(prodList.get(i).getKey()+"\t\t"+prodList.get(i-1).getValue());
////                  //  i--;
////                }
////                else
////                    System.out.println(prodList.get(i).getKey()+"\t\t"+prodList.get(i).getValue());
////            }
////            else
////            System.out.println(prodList.get(i).getKey()+"\t\t"+prodList.get(i).getValue());
////        
////        }
    }
    }
    
    
    
    public void topThreeSalesPeople(){
      Map<Integer, Order> order = Datastore.getInstance().getOrders();
        Map<Integer, Product> prod = Datastore.getInstance().getProducts();
        Map<Integer, Integer>sales = new HashMap<>();
        int sale = 0;
        int target =0;
        int profit =0;
        int quantity =0;
        for(Order o: order.values()){
            Product p = prod.get(o.getItem().getProductId());
            if(o.getItem().getSalesPrice()>p.getTargetPrice()){
            if(sales.containsKey(o.getSupplierId())){
                sale = o.getItem().getSalesPrice();
                target = p.getTargetPrice();
                quantity = o.getItem().getQuantity();
                profit = ((sale-target)*quantity)+sales.get(o.getSupplierId());
                
            }
            else{
                sale = o.getItem().getSalesPrice();
                target = p.getTargetPrice();
                quantity = o.getItem().getQuantity();
                profit = (sale-target)*quantity;
            }
            sales.put(o.getSupplierId(), profit);
            }
          
        }
       List<Map.Entry<Integer, Integer>>saleList = new LinkedList<Map.Entry<Integer, Integer>>(sales.entrySet());
        Collections.sort(saleList, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
                return Double.compare(t1.getValue(), t.getValue());
            }
        });
         System.out.println("SalesPerson Id"+"\t"+"Profit");
        for(int i=0;i<saleList.size()&&i<3;i++){
              System.out.println(saleList.get(i).getKey()+"\t\t"+saleList.get(i).getValue());
        }
        System.out.println("_____________________________________________________________");
            int sum =0;
        for(int j=0;j<sales.size();j++){
            sum +=sales.get(j);
        }
        System.out.println("Question number 4 - Total profit for the year\n");
        System.out.println("Total Revenue: "+sum);
    }
    
    
    public void topThreeCustomers(){
        Map<Integer, Order> order = Datastore.getInstance().getOrders();
        Map<Integer, Product> prod = Datastore.getInstance().getProducts();
        Map<Integer, Integer> cust = new HashMap<>(); 
        int value =0;
        int alreadyValue =0;
        for(Order o: order.values()){
           Product p = prod.get(o.getItem().getProductId());
           if(o.getItem().getSalesPrice()>p.getTargetPrice()){
           if(cust.containsKey(o.getCustomerId())){
               alreadyValue = cust.get(o.getCustomerId());
               value = (o.getItem().getSalesPrice()-p.getTargetPrice())+alreadyValue;
               
           }
           else{
               int sales = o.getItem().getSalesPrice();
               int target = p.getTargetPrice();
               value = sales - target;
           }
           cust.put(o.getCustomerId(), value);
           }
        }
        List<Map.Entry<Integer, Integer>>custList = new LinkedList<Map.Entry<Integer, Integer>>(cust.entrySet());
        Collections.sort(custList, new Comparator<Map.Entry<Integer, Integer>>() {
            @Override
            public int compare(Map.Entry<Integer, Integer> t, Map.Entry<Integer, Integer> t1) {
                return Double.compare(t1.getValue(), t.getValue());
            }
        });
         System.out.println("Customer Id"+"\t"+"Absolute value of the difference between the sale price and target price of the products");
        for(int i=0;i<custList.size()&&i<3;i++){
              System.out.println(custList.get(i).getKey()+"\t\t"+custList.get(i).getValue());
        }
    
    }
    
    //adding round method
    public static double round(double value, int places) {
    if (places < 0) throw new IllegalArgumentException();
    long factor = (long) Math.pow(10, places);
    value = value * factor;
    long tmp = Math.round(value);
    return (double) tmp / factor;
}
    
    
    
    //question 4 
    
    
    public void displayAverageSalesPrice(){
        Map<Integer, Order> order = Datastore.getInstance().getOrders(); // with this access the order map
        Map<Integer, Product> product = Datastore.getInstance().getProducts(); //with this access the product map
        Map<Integer, Integer> salesMap = new HashMap<>(); //product id and sales average 
        Map<Integer, Integer> counter = new HashMap<>();
        Map<Integer, Integer> average = new HashMap<>();
    int count =0;
    int sales=0;
     int avg =0;
// Map <Integer, List<Table>> smallList = new HashMap<>(); 
    for(Order i: order.values()){
        if(salesMap.containsKey(i.getItem().getProductId())){
            

           sales =  i.getItem().getSalesPrice()+salesMap.get(i.getItem().getProductId());
       
count = counter.get(i.getItem().getProductId())+1;
avg=sales/count;

        }
        else{
            sales  = i.getItem().getSalesPrice();
        count =1;
        avg=sales/count;
        }
        counter.put(i.getItem().getProductId(), count);
        salesMap.put(i.getItem().getProductId(), sales);
        average.put(i.getItem().getProductId(),avg);
    
        
        
    }

    List<Map.Entry<Integer, Integer>> averageList = new LinkedList<Map.Entry<Integer, Integer>>(average.entrySet());
List<Table> smallList = new ArrayList <Table>(average.size());
List<Table> bigList = new ArrayList <Table>(average.size());

//       System.out.println("_____________________________________________________________");
         System.out.println("Table for  original data(Average Sales price lower than target price)\n");
       for(int x=0;x<averageList.size();x++){
            if(averageList.get(x).getValue()<=product.get(x).getTargetPrice()){
                //smallList.add(averageList.get(x).getKey(), new Table(averageList.get(x).getValue(), product.get(x).getTargetPrice(), averageList.get(x).getKey(), averageList.get(x).getValue()-product.get(x).getTargetPrice()));
            Table t = new Table(averageList.get(x).getValue(), product.get(x).getTargetPrice(), averageList.get(x).getKey(), averageList.get(x).getValue()-product.get(x).getTargetPrice());
            smallList.add(t);
            }
                
       }
//       System.out.println("_____________________________________________________________");
        System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference(Avg Sales - Target)");
       Collections.sort(smallList, new Comparator<Table>() {
          

            @Override
            public int compare(Table t, Table t1) {
             return Double.compare(t1.getDifference(),t.getDifference());
            }
        });
        for(int z=0; z<smallList.size();z++){
            System.out.println(smallList.get(z));
        }
        System.out.println("------------------------------------------------------------");
         System.out.println("Table for  original data(Average Sales price greater than target price)\n");
       for(int x=0;x<averageList.size();x++){
            if(averageList.get(x).getValue()>product.get(x).getTargetPrice()){
                //smallList.add(averageList.get(x).getKey(), new Table(averageList.get(x).getValue(), product.get(x).getTargetPrice(), averageList.get(x).getKey(), averageList.get(x).getValue()-product.get(x).getTargetPrice()));
            Table t = new Table(averageList.get(x).getValue(), product.get(x).getTargetPrice(), averageList.get(x).getKey(), averageList.get(x).getValue()-product.get(x).getTargetPrice());
            bigList.add(t);
            }
                
       }
       System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference(Avg Sales - Target)");
       Collections.sort(bigList, new Comparator<Table>() {
          

            @Override
            public int compare(Table t, Table t1) {
             return Double.compare(t1.getDifference(),t.getDifference());
            }
        });
        for(int z=0; z<bigList.size();z++){
            System.out.println(bigList.get(z));
        }
      
    }
    
    //adding approac 1 from question 5b
    public void modification(){
        Map<Integer, Order> order = Datastore.getInstance().getOrders(); // with this access the order map
        Map<Integer, Product> product = Datastore.getInstance().getProducts(); //with this access the product map
        Map<Integer, Integer> salesMap = new HashMap<>(); //product id and sales average 
        Map<Integer, Integer> counter = new HashMap<>();
        Map<Integer, Integer> average = new HashMap<>();
    int count =0;
    int sales=0;
     int avg =0;

    for(Order i: order.values()){
        if(salesMap.containsKey(i.getItem().getProductId())){
            

           sales =  i.getItem().getSalesPrice()+salesMap.get(i.getItem().getProductId());
       
count = counter.get(i.getItem().getProductId())+1;
avg=sales/count;

        }
        else{
            sales  = i.getItem().getSalesPrice();
        count =1;
        avg=sales/count;
        }
        counter.put(i.getItem().getProductId(), count);
        salesMap.put(i.getItem().getProductId(), sales);
        average.put(i.getItem().getProductId(),avg);
      
    }
     List<Map.Entry<Integer, Integer>> averageList = new LinkedList<Map.Entry<Integer, Integer>>(average.entrySet());
     List<ModifiedTable> smallList = new ArrayList <ModifiedTable>(average.size());
     List<ModifiedTable> bigList = new ArrayList <ModifiedTable>(average.size());
double error =0;
double newError =0;
int modifiedTargetPrice=0;


 System.out.println("_____________________________________________________________");
         System.out.println("Table for  Modified data(Average Sales price lower than target price)");
       for(int x=0;x<averageList.size();x++){
            if(averageList.get(x).getValue()<=product.get(x).getTargetPrice()){
                error = (((double)(product.get(x).getTargetPrice()-averageList.get(x).getValue())/(double)averageList.get(x).getValue())*100);
                BigDecimal bde = new BigDecimal(error).setScale(2, RoundingMode.HALF_UP);     
                error=bde.doubleValue();
           
               if(error>5){
               newError = error-5;
               modifiedTargetPrice = (int) (product.get(x).getTargetPrice()-((product.get(x).getTargetPrice()*newError)/100));
           } 
               else{
                   modifiedTargetPrice=product.get(x).getTargetPrice();
               }
                newError = (((double)(modifiedTargetPrice-averageList.get(x).getValue())/(double)averageList.get(x).getValue())*100);
                BigDecimal bdn = new BigDecimal(newError).setScale(2, RoundingMode.HALF_UP);
                newError = bdn.doubleValue();
                ModifiedTable t = new ModifiedTable(averageList.get(x).getValue(), product.get(x).getTargetPrice(), averageList.get(x).getKey(), averageList.get(x).getValue()-product.get(x).getTargetPrice(),error, modifiedTargetPrice, newError);
            smallList.add(t);
            }
        
                
       }
       Collections.sort(smallList, new Comparator<ModifiedTable>() {
            @Override
            public int compare(ModifiedTable t, ModifiedTable t1) {
               return Double.compare(t1.getDifference(),t.getDifference());
            }
        });
       System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference(Avg Sales - Target)"+"\t"+"Modified Target Price"+"\t"+"Old Error%"+"\t"+"New Error%");
 for(int z=0; z<smallList.size();z++){
            System.out.println(smallList.get(z));
        }
 System.out.println("_____________________________________________________________");
         System.out.println("Table for  Modified data(Average Sales price greater than target price)");
 for(int x=0;x<averageList.size();x++){
            if(averageList.get(x).getValue()>product.get(x).getTargetPrice()){
                error = (((double)(product.get(x).getTargetPrice()-averageList.get(x).getValue())/(double)averageList.get(x).getValue())*100);
                  BigDecimal bde = new BigDecimal(error).setScale(2, RoundingMode.HALF_UP);         
                error=bde.doubleValue();
                          if(error<-5){
               newError = error+5;
               modifiedTargetPrice = (int) (product.get(x).getTargetPrice()+((product.get(x).getTargetPrice()*newError)/100));
           }
           
               else{
                   modifiedTargetPrice=product.get(x).getTargetPrice();
               }
                          
                newError = (((double)(modifiedTargetPrice-averageList.get(x).getValue())/(double)averageList.get(x).getValue())*100);
               BigDecimal bdn = new BigDecimal(newError).setScale(2, RoundingMode.HALF_UP);
                newError = bdn.doubleValue();
                ModifiedTable t = new ModifiedTable(averageList.get(x).getValue(), product.get(x).getTargetPrice(), averageList.get(x).getKey(), averageList.get(x).getValue()-product.get(x).getTargetPrice(),error, modifiedTargetPrice,newError);
            bigList.add(t);
            }
        
                
       }
       Collections.sort(bigList, new Comparator<ModifiedTable>() {
            @Override
            public int compare(ModifiedTable t, ModifiedTable t1) {
               return Double.compare(t1.getDifference(),t.getDifference());
            }
        });
 
        System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference(Avg Sales - Target)"+"\t"+"Modified Target Price"+"\t"+"Old Error%"+"\t"+"New Error%");
 for(int z=0; z<bigList.size();z++){
         
     System.out.println(bigList.get(z));

        }
    }
    
    //adding approach 2 for question 5
    public void dispayModifiedDataApproach2()
    {
        //System.out.println("Question 5  - approach 2");
        Map<Integer, Order> order = Datastore.getInstance().getOrders();
        Map<Integer, Product> prod = Datastore.getInstance().getProducts();
        Map<Integer, Integer> salesMap = new HashMap<>(); //product id and sales average 
        Map<Integer, Integer> counter1 = new HashMap<>();
        Map<Integer, Integer> average = new HashMap<>();
        Map<Integer, ModifiedTable>counter = new HashMap<>();
        
        int c1=0,c2=0;
        int newvalue=0;
        ModifiedTable t= null;
        ModifiedTable t1= null;
        ModifiedTable tfinal = null;
        int count =0;
        int sales=0;
        int avg =0;
 
    for(Order i: order.values()){
        if(salesMap.containsKey(i.getItem().getProductId())){
           sales =  i.getItem().getSalesPrice()+salesMap.get(i.getItem().getProductId());
           count = counter1.get(i.getItem().getProductId())+1;
           avg=sales/count;
        }
        else{
            sales  = i.getItem().getSalesPrice();
        count =1;
        avg=sales/count;
        }
        counter1.put(i.getItem().getProductId(), count);
        salesMap.put(i.getItem().getProductId(), sales);
        average.put(i.getItem().getProductId(),avg);
   }

    List<Map.Entry<Integer, Integer>> averageList = new LinkedList<Map.Entry<Integer, Integer>>(average.entrySet());
    
        for(Order o:order.values())
        {
            Product p = prod.get(o.getItem().getProductId());
            
                    if(counter.containsKey(o.getItem().getProductId()))
                    {
                       t = counter.get(o.getItem().getProductId());
                        if (o.getItem().getSalesPrice() > p.getTargetPrice())
                        {
                            c1 = t.getCounter1()+1;
                            c2 = t.getCounter2();                              
                        }
                        else
                        {
                            c1 = t.getCounter1();
                            c2 = t.getCounter2()+1;
                      }
                         t = new ModifiedTable(o.getItem().getProductId(),c1, c2);
                         // t = new ModifiedTable(average.get(o.getItem().getProductId()),p.getTargetPrice(),o.getItem().getProductId(),o.getItem().getSalesPrice()- p.getTargetPrice(),0,10); 
                    }
                    else
                    {
                        if (o.getItem().getSalesPrice() > p.getTargetPrice())
                        {
                            c1 =1;
                            c2=0;
                        }    
                        else
                        {
                            c1 =1;
                            c2=0;
                        }
                        t = new ModifiedTable(o.getItem().getProductId(),c1, c2);                        
                    }
                    counter.put(o.getItem().getProductId(), t);
                    
                }
                //error calculation part
                int newcalcvalue = 0;
                double calcerror = 0.0;
                // int target = 10;//1
                //int target1 = 200;//2
               List<ModifiedTable> modifiedTable = new ArrayList<ModifiedTable>();
               //System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference"+"\t"+"Error"+"\t"+"New Target Price");
                for(ModifiedTable m: counter.values()){//isme product id 0 he aaraha hai
                        Product p = prod.get(m.getProductId());
                        if(m.getCounter1() > m.getCounter2()){
                            newcalcvalue = (p.getTargetPrice() + (average.get(m.getProductId())-p.getTargetPrice())/2);
                            //newcalcvalue = (target + (average.get(m.getProductId())-target)/2);
                            calcerror = ((double)(newcalcvalue - average.get(m.getProductId()))/(double)average.get(m.getProductId()))*100 ;
                            while(calcerror < -5){
                            newcalcvalue = (newcalcvalue + (average.get(m.getProductId())- newcalcvalue)/2);
                            //newcalcvalue = (newcalcvalue + (average.get(m.getProductId())-newcalcvalue)/2);   
                            calcerror = ((double)(newcalcvalue - average.get(m.getProductId()))/(double)average.get(m.getProductId()))*100 ; 
                            }   
                            
                            t = new ModifiedTable((average.get(m.getProductId())),p.getTargetPrice(),m.getProductId(),((average.get(m.getProductId()))- p.getTargetPrice()), calcerror, newcalcvalue  );
                            counter.put(m.getProductId(), t);
                            //System.out.println("\t"+m.getProductId()+"\t\t"+(average.get(m.getProductId()))+"\t\t"+p.getTargetPrice()+"\t\t"+round(((average.get(m.getProductId()))- p.getTargetPrice()),2)+"\t\t"+round(calcerror,2)+"\t\t"+newcalcvalue  );
                        }
                 
                        if(m.getCounter1() <= m.getCounter2()){
                            newcalcvalue = (p.getTargetPrice() - (p.getTargetPrice()-average.get(m.getProductId()))/2);
                            //newcalcvalue = (target1 - (target1-average.get(m.getProductId()))/2);
                            calcerror = ((double)(newcalcvalue - average.get(m.getProductId()))/(double)average.get(m.getProductId()))*100 ;
           
                            while(calcerror > 5 ){
                            //newcalcvalue = newcalcvalue - ((newcalcvalue + average.get(m.getProductId()))/2);
                            newcalcvalue = (newcalcvalue - (newcalcvalue-average.get(m.getProductId()))/2);
                            calcerror = ((double)(newcalcvalue - average.get(m.getProductId()))/(double)average.get(m.getProductId()))*100 ;
                            
                            }
                            
                            t = new ModifiedTable((average.get(m.getProductId())),p.getTargetPrice(),m.getProductId(),((average.get(m.getProductId()))- p.getTargetPrice()), calcerror, newcalcvalue  );
                            counter.put(m.getProductId(), t);
                            //System.out.println(t);
                            //System.out.println("\t"+m.getProductId()+"\t\t"+(average.get(m.getProductId()))+"\t\t"+p.getTargetPrice()+"\t\t"+round(((average.get(m.getProductId()))- p.getTargetPrice()),2)+"\t\t"+round(calcerror,2)+"\t\t"+newcalcvalue  );
                        }       
                    }
                //System.out.println(counter.keySet());
                //System.out.println(counter.values());
                List<ModifiedTable> list1 = new ArrayList<>();
                List<ModifiedTable> list2 = new ArrayList<>();
                for(ModifiedTable n: counter.values()){
                    if(n.getAverageSalesPrice() <= n.getTargetPrice()){
                        list1.add(n);
                    }
                    else if(n.getAverageSalesPrice() > n.getTargetPrice()){
                    list2.add(n);
                }
                }
                //System.out.println("Table for products with average sale price less than target price");
                //System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference"+"\t"+"Error"+"\t"+"New Target Price");
                
                //System.out.println(list1);
                /*for(ModifiedTable a: list1){
                    //System.out.println("\t"+a.getProductId()+"\t\t"+a.getAverageSalesPrice()+"\t\t"+a.getTargetPrice()+"\t\t"+a.getDifference()+"\t\t"+round(a.getError(),2)+"\t\t"+a.getModifiedTargetPrice()  );
                
                }
                System.out.println("Table for products with average sale price greater than target price");
                //System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference"+"\t"+"Error"+"\t"+"New Target Price");
                //System.out.println(list2);
                for(ModifiedTable a: list2){
                   // System.out.println("\t"+a.getProductId()+"\t\t"+a.getAverageSalesPrice()+"\t\t"+a.getTargetPrice()+"\t\t"+a.getDifference()+"\t\t"+round(a.getError(),2)+"\t\t"+a.getModifiedTargetPrice()  );
                
                }*/
                
                List<Map.Entry<Integer, ModifiedTable>> mList = new LinkedList<Map.Entry<Integer, ModifiedTable>>(counter.entrySet());
               Collections.sort(mList, new Comparator<Map.Entry<Integer, ModifiedTable>>() {
            @Override
            public int compare(Map.Entry<Integer, ModifiedTable> t, Map.Entry<Integer, ModifiedTable> t1) {
               return Double.compare(t1.getValue().getDifference(), t.getValue().getDifference());
            }
        }); 

        
        System.out.println("Modified Table for products with average sale price less than target price");
        System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference"+"\t"+"Error"+"\t"+"New Target Price");        
                for(int i=0;i<mList.size();i++){
                    if(mList.get(i).getValue().getAverageSalesPrice()< mList.get(i).getValue().getTargetPrice()){
                    System.out.println("\t"+mList.get(i).getValue().getProductId()+"\t\t"+mList.get(i).getValue().getAverageSalesPrice()+"\t\t"+mList.get(i).getValue().getTargetPrice()+"\t\t"+mList.get(i).getValue().getDifference()+"\t\t"+round(mList.get(i).getValue().getError(),2)+"\t\t"+mList.get(i).getValue().getModifiedTargetPrice()  );
                    }
                }     
                
        System.out.println("Modified Table for products with average sale price greater than target price");
        System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference"+"\t"+"Error"+"\t"+"New Target Price");        
                for(int i=0;i<mList.size();i++){
                    if(mList.get(i).getValue().getAverageSalesPrice()>= mList.get(i).getValue().getTargetPrice()){
                    System.out.println("\t"+mList.get(i).getValue().getProductId()+"\t\t"+mList.get(i).getValue().getAverageSalesPrice()+"\t\t"+mList.get(i).getValue().getTargetPrice()+"\t\t"+mList.get(i).getValue().getDifference()+"\t\t"+round(mList.get(i).getValue().getError(),2)+"\t\t"+mList.get(i).getValue().getModifiedTargetPrice()  );
                    }
                }           
    }
    
    
    
    
    //Question 5 - approach 3
    public void modifiedtargetvalue3()
    {
        
        Map<Integer, Order> order = Datastore.getInstance().getOrders();
        Map<Integer, Product> prod = Datastore.getInstance().getProducts();
        Map<Integer, Integer> salesMap = new HashMap<>(); //product id and sales average 
        Map<Integer, Integer> counter1 = new HashMap<>();
        Map<Integer, Integer> average = new HashMap<>();
        Map<Integer, ModifiedTable>counter = new HashMap<>();
        
        int c1=0,c2=0;
        int newvalue=0;
        ModifiedTable t= null;
        ModifiedTable t1= null;
        ModifiedTable tfinal = null;
        
        int count =0;
        int sales=0;
        int avg =0;
        double calcerror =0.0;
        double olderror =0.0;
        double newerrorvalueRounded =0.0;
        double olderrorvalueRounded=0.0;
// Map <Integer, List<Table>> smallList = new HashMap<>(); 
    for(Order i: order.values()){
        if(salesMap.containsKey(i.getItem().getProductId())){
            

           sales =  i.getItem().getSalesPrice()+salesMap.get(i.getItem().getProductId());
           count = counter1.get(i.getItem().getProductId())+1;
           avg=sales/count;

        }
        else{
            sales  = i.getItem().getSalesPrice();
        count =1;
        avg=sales/count;
        }
        counter1.put(i.getItem().getProductId(), count);
        salesMap.put(i.getItem().getProductId(), sales);
        average.put(i.getItem().getProductId(),avg);
        }
    
     List<Map.Entry<Integer, Integer>> averageList = new LinkedList<Map.Entry<Integer, Integer>>(average.entrySet());
    

     for(Product p : prod.values())
     {
         int a = average.get(p.getProductId());
         
         
         if( a >= p.getTargetPrice())
         {
             if((a-(int)(0.05*a)) > p.getTargetPrice())
             {
                 newvalue = (a-(int)(0.05*a));
                 calcerror = (((double)(newvalue - a))/(double)a)*100 ;
                 olderror = ((double)(p.getTargetPrice() - a)/(double)a)*100;
                 newerrorvalueRounded = Math.round(calcerror * 100D)/100D;
                 olderrorvalueRounded = Math.round(olderror * 100D)/100D;
             
             }
             else
             {
                 newvalue = p.getTargetPrice();
                  calcerror = (((double)(newvalue - a))/(double)a)*100 ;
                  olderror = ((double)(p.getTargetPrice() - a)/(double)a)*100;
                  newerrorvalueRounded = Math.round(calcerror * 100D)/100D;
                 olderrorvalueRounded = Math.round(olderror * 100D)/100D;
                 
                  
             }
              t = new ModifiedTable((average.get(p.getProductId())),p.getTargetPrice(),p.getProductId(),((average.get(p.getProductId()))- p.getTargetPrice()),olderrorvalueRounded, newvalue,newerrorvalueRounded);
              counter.put(p.getProductId(), t);
             // System.out.println("\t"+p.getProductId()+"\t\t"+average.get(p.getProductId())+"\t\t"+p.getTargetPrice()+"\t\t\t"+(average.get(p.getProductId())- p.getTargetPrice())+"\t\t\t\t"+newvalue+"\t\t"+olderrorvalueRounded+"\t\t"+newerrorvalueRounded+"\t"); 
       
         }
         
         else 
         {
             if((a+(int)(0.05*a)) < p.getTargetPrice())
             {
                 newvalue = (a+(int)(0.05*a));
               calcerror = (((double)(newvalue - a))/(double)a)*100 ;
               olderror = ((double)(p.getTargetPrice() - a)/(double)a)*100;
               newerrorvalueRounded = Math.round(calcerror * 100D)/100D;
                 olderrorvalueRounded = Math.round(olderror * 100D)/100D;
                 
                   
             }
             else
             {
                 newvalue = p.getTargetPrice();
                 calcerror = (((double)(newvalue - a))/(double)a)*100 ;
                 olderror = ((double)(p.getTargetPrice() - a)/(double)a)*100;
                 newerrorvalueRounded = Math.round(calcerror * 100D)/100D;
                 olderrorvalueRounded = Math.round(olderror * 100D)/100D;
                 
                  
             }
             
              t = new ModifiedTable((average.get(p.getProductId())),p.getTargetPrice(),p.getProductId(),((average.get(p.getProductId()))- p.getTargetPrice()),olderrorvalueRounded, newvalue,newerrorvalueRounded);
              counter.put(p.getProductId(), t);
              //System.out.println("\t"+p.getProductId()+"\t\t"+average.get(p.getProductId())+"\t\t"+p.getTargetPrice()+"\t\t\t"+(average.get(p.getProductId())- p.getTargetPrice())+"\t\t\t\t"+newvalue+"\t\t"+olderrorvalueRounded+"\t\t"+newerrorvalueRounded+"\t"); 
       
         }
         
         
     }
     List<Map.Entry<Integer, ModifiedTable>> mList = new LinkedList<Map.Entry<Integer, ModifiedTable>>(counter.entrySet());
               Collections.sort(mList, new Comparator<Map.Entry<Integer, ModifiedTable>>() {
            @Override
            public int compare(Map.Entry<Integer, ModifiedTable> t, Map.Entry<Integer, ModifiedTable> t1) {
               return Double.compare(t1.getValue().getDifference(), t.getValue().getDifference());
            }
        });
               
               System.out.println("Table for  Modified data(Average Sales price greater than target price)");
               System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference(Avg Sales - Target)"+"\t"+"Modified Target Price"+"\t"+"Old Error%"+"\t"+"New Error%");
                for(int i=0;i<mList.size();i++)
               {
                    if(mList.get(i).getValue().getAverageSalesPrice()>= mList.get(i).getValue().getTargetPrice())
                    {
                    System.out.println("\t"+mList.get(i).getValue().getProductId()+"\t\t"+mList.get(i).getValue().getAverageSalesPrice()+"\t\t"+mList.get(i).getValue().getTargetPrice()+"\t\t\t"+((mList.get(i).getValue().getAverageSalesPrice())-(mList.get(i).getValue().getTargetPrice()))+"\t\t\t\t"+mList.get(i).getValue().getModifiedTargetPrice()+"\t\t"+mList.get(i).getValue().getError()+"\t\t"+mList.get(i).getValue().getNewError()+"\t"); 
                    
                    }
                }
               
               System.out.println("Table for  Modified data(Average Sales price less than target price)");
               System.out.println("Product Id: "+"\t"+"Average Sales Price"+"\tTarget Price"+"\t"+"Difference(Avg Sales - Target)"+"\t"+"Modified Target Price"+"\t"+"Old Error%"+"\t"+"New Error%");

               for(int i=0;i<mList.size();i++)
               { 
                    if(mList.get(i).getValue().getAverageSalesPrice()< mList.get(i).getValue().getTargetPrice())
                    {
                    System.out.println("\t"+mList.get(i).getValue().getProductId()+"\t\t"+mList.get(i).getValue().getAverageSalesPrice()+"\t\t"+mList.get(i).getValue().getTargetPrice()+"\t\t\t"+((mList.get(i).getValue().getAverageSalesPrice())-(mList.get(i).getValue().getTargetPrice()))+"\t\t\t\t"+mList.get(i).getValue().getModifiedTargetPrice()+"\t\t"+mList.get(i).getValue().getError()+"\t\t"+mList.get(i).getValue().getNewError()+"\t"); 
                    
                    }
                }
     
               
               
     
    
    }
     
}
