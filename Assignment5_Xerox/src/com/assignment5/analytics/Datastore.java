/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.analytics;

import com.assignment5.entities.Order;
import com.assignment5.entities.Product;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Sid
 */
public class Datastore {
    
    private static Datastore datastore;
    
    private Map<Integer, Product> products;
    private Map<Integer, Order> orders;

    public Map<Integer, Order> getOrders() {
        return orders;
    }

    public void setOrders(Map<Integer, Order> orders) {
        this.orders = orders;
    }
    
    private Datastore(){
        products = new HashMap<>();
        orders = new HashMap<>();
    }
    
     public static Datastore getInstance(){
        if(datastore == null)
            datastore = new Datastore();
        return datastore;
    }

    public static Datastore getDatastore() {
        return datastore;
    }

    public static void setDatastore(Datastore datastore) {
        Datastore.datastore = datastore;
    }

    public Map<Integer, Product> getProducts() {
        return products;
    }

    public void setProducts(Map<Integer, Product> products) {
        this.products = products;
    }
   
}
