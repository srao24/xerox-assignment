/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author Sid
 */
public class Table {
     int averageSalesPrice;
        int targetPrice;
        int productId;
        int difference;
        

        public int getAverageSalesPrice() {
            return averageSalesPrice;
        }

        public void setAverageSalesPrice(int averageSalesPrice) {
            this.averageSalesPrice = averageSalesPrice;
        }

        public int getTargetPrice() {
            return targetPrice;
        }

        public void setTargetPrice(int targetPrice) {
            this.targetPrice = targetPrice;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public int getDifference() {
            return difference;
        }

        public void setDifference(int difference) {
            this.difference = difference;
        }

           
        public Table(int averageSalesPrice,int targetPrice, int productId, int difference){
              this.averageSalesPrice=averageSalesPrice;
       this.targetPrice=targetPrice;
        this.productId=productId;
        this.difference=difference;
        }
       
        @Override
        public String toString(){
           return "\t"+productId+"\t\t"+averageSalesPrice+"\t\t\t"+targetPrice+"\t"+difference+" "; 
        }
    
}
