/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

/**
 *
 * @author Sid
 */
public class ModifiedTable {
      int averageSalesPrice;
        int targetPrice;
        int productId;
        int difference;
        double error;
        double newError;
        int modifiedTargetPrice;
        int counter1;
        int counter2;

    public double getNewError() {
        return newError;
    }

    public void setNewError(double newError) {
        this.newError = newError;
    }

    public int getCounter1() {
        return counter1;
    }

    public void setCounter1(int counter1) {
        this.counter1 = counter1;
    }

    public int getCounter2() {
        return counter2;
    }

    public void setCounter2(int counter2) {
        this.counter2 = counter2;
    }
             

        public int getAverageSalesPrice() {
            return averageSalesPrice;
        }

        public void setAverageSalesPrice(int averageSalesPrice) {
            this.averageSalesPrice = averageSalesPrice;
        }

        public int getTargetPrice() {
            return targetPrice;
        }

        public void setTargetPrice(int targetPrice) {
            this.targetPrice = targetPrice;
        }

        public int getProductId() {
            return productId;
        }

        public void setProductId(int productId) {
            this.productId = productId;
        }

        public int getDifference() {
            return difference;
        }

        public void setDifference(int difference) {
            this.difference = difference;
        }

    public double getError() {
        return error;
    }

    public void setError(double error) {
        this.error = error;
    }

    public int getModifiedTargetPrice() {
        return modifiedTargetPrice;
    }

    public void setModifiedTargetPrice(int modifiedTargetPrice) {
        this.modifiedTargetPrice = modifiedTargetPrice;
    }
        
     
         public ModifiedTable(int averageSalesPrice,int targetPrice, int productId, int difference, double error, int modifiedTargetPrice, double newError){
              this.averageSalesPrice=averageSalesPrice;
       this.targetPrice=targetPrice;
        this.productId=productId;
        this.difference=difference;
        this.error = error;
        this.modifiedTargetPrice = modifiedTargetPrice;
        this.newError = newError;
        }
         
         public ModifiedTable(int averageSalesPrice,int targetPrice, int productId, int difference, double error, int modifiedTargetPrice){
              this.averageSalesPrice=averageSalesPrice;
       this.targetPrice=targetPrice;
        this.productId=productId;
        this.difference=difference;
        this.error = error;
        this.modifiedTargetPrice = modifiedTargetPrice;
        
        }
         
        public ModifiedTable(int productId,int counter1, int counter2){
            this.counter1 = counter1;
            this.counter2= counter2;
            this.productId=productId;
        } 
        @Override
        public String toString(){
           return "\t"+productId+"\t\t"+averageSalesPrice+"\t\t\t"+targetPrice+"\t"+difference+"\t\t\t\t"+modifiedTargetPrice+"\t\t\t"+error+"\t\t"+newError; 
        }
    
    
}
