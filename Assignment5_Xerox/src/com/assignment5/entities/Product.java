/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.assignment5.entities;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author kasai
 */
public class Product {
    private int productId;
    private int minPrice;
    private int maxPrice;
    private int targetPrice;
    //private String marketDescription;
private List<Order> orders;    

    public Product(int productId, int minPrice, int maxPrice, int targetPrice) {
        this.productId = productId;
        this.minPrice = minPrice;
        this.maxPrice = maxPrice;
        this.targetPrice = targetPrice;
       // this.marketDescription = marketDescription;
        this.orders = new ArrayList<Order>();
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(int minPrice) {
        this.minPrice = minPrice;
    }

    public int getMaxPrice() {
        return maxPrice;
    }

    public void setMaxPrice(int maxPrice) {
        this.maxPrice = maxPrice;
    }

    public int getTargetPrice() {
        return targetPrice;
    }

    public void setTargetPrice(int targetPrice) {
        this.targetPrice = targetPrice;
    }

//    public String getMarketDescription() {
//        return marketDescription;
//    }
//
//    public void setMarketDescription(String marketDescription) {
//        this.marketDescription = marketDescription;
//    }

    public List<Order> getOrders() {
        return orders;
    }

    public void setOrders(List<Order> orders) {
        this.orders = orders;
    }
    
    @Override
    public String toString(){
      return "Product{" + "id = " + productId + ", Min Price = " + minPrice + ", Max Price = " + maxPrice + ", Target Price = " +targetPrice+"Order="+orders+'}';
    }
    
}
